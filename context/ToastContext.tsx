"use client";
import { ToastContextType } from "@/utils/definitions";
import React, { ReactNode, createContext, useContext, useState } from "react";



const defaultValue: ToastContextType = {
  showToast: () => {},
  visibility: false,
  msg: "",
  type: "",
};
const ToastContext = createContext(defaultValue);

const ToastProvider = ({ children }: { children: ReactNode }) => {
  const [visibility, setVisibility] = useState(false);
  const [msg, setMsg] = useState("");
  const [type, setType] = useState("error");

  const showToast = (msg: string, type: "error" | "success") => {
    setVisibility(true);
    setMsg(msg);
    setType(type);

    setTimeout(() => {
      setVisibility(false);
    }, 3000);
  };

  return (
    <ToastContext.Provider value={{ showToast, visibility, msg, type }}>
      {children}
    </ToastContext.Provider>
  );
};

export default ToastProvider;

export function useToast() {
  const context = useContext(ToastContext);
  return { ...context };
}
