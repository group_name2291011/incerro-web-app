import { StaticImageData } from "next/image";

export type IndustryCardPropType = {
    id:number;
    title: string;
    description: string;
    image: StaticImageData;
  };
 export type ToastContextType = {
    showToast: (msg: string, type: "error" | "success") => void;
    visibility: boolean;
    msg: string;
    type: string;
  };