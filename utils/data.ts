import ecomImg from "@/assets/ecom.png";
import aiImg from "@/assets/artificial-intelligence.png";
import adImg from "@/assets/advert.png";
import fintechImg from "@/assets/fintech.png";
import healthcareImg from "@/assets/healthcare.jpg";

const industriesData = [
  {
    id: 1,
    title: "Healthcare",
    description:
      "Serving startups, medical institutions and various stakeholders of the healthcare industry with our expertise in building HIPAA compliant applications",
    image: healthcareImg,
  },
  {
    id: 2,
    title: "E-Commerce",
    description:
      "Leading innovation in the e-commerce industry with our expertise in building scalable applications",
    image: ecomImg,
  },
  {
    id: 3,
    title: "Artificial Intelligence",
    description:
      "Innovating solutions for the advertising industry to help them reach their target audience",
    image: aiImg,
  },
  {
    id: 4,
    title: "Advertisement",
    description:
      "Be it a ChatGPT based Chatbot or integrating AI in your existing business, we have got you covered",
    image: adImg,
  },
  {
    id: 5,
    title: "Fintech",
    description:
      "Transforming Fintech as AI and blockchain emerging as the next big thing in financial services",
    image: fintechImg,
  },
];

export default industriesData;
