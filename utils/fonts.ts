import { Open_Sans, Poppins, Montserrat } from 'next/font/google';
 
export const montserrat = Montserrat({ subsets: ['latin'] });
export const openSans = Open_Sans({ subsets: ['latin'] });
export const poppins = Poppins({ weight: ['400', '500'], subsets: ['latin'] });
