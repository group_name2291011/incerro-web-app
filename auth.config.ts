import type { NextAuthConfig } from "next-auth";

export const authConfig = {
  callbacks: {
    async jwt({ token, user }) {
      return { ...token, ...user };
    },

    async session({ session, token }) {
      return { ...session, role: token.role };
    },
    
    authorized({ auth, request: { nextUrl } }) {
      const isLoggedIn = !!auth?.user;

      const isOnProtectedRoute =
        !nextUrl.pathname.startsWith("/api/auth/signin"); // every route is protected except /api/auth/signin

      if (isOnProtectedRoute) {
        if (isLoggedIn) return true;
        return false; // redirect to login page
      } else if (isLoggedIn) {
        return Response.redirect(new URL("/", nextUrl)); //redirect to home page
      }
      return true;
    },
  },
  providers: [], // Add providers with an empty array for now
} satisfies NextAuthConfig;
