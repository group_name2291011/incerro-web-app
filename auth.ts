import NextAuth from "next-auth";
import { authConfig } from "./auth.config";
import Credentials from "next-auth/providers/credentials";
import GoogleProvider from "next-auth/providers/google";
import GitHubProvider from "next-auth/providers/github";
import { z } from "zod";

export const { handlers, auth, signIn, signOut } = NextAuth({
  ...authConfig,
  providers: [
    GoogleProvider({
        clientId: process.env.GOOGLE_CLIENT_ID,
        clientSecret: process.env.GOOGLE_CLIENT_SECRET,
      }),
      GitHubProvider({
        clientId: process.env.GITHUB_ID,
        clientSecret: process.env.GITHUB_SECRET
      }),
    Credentials({
      name: "Email",
      credentials: {
        email: { label: "Email", type: "text" },
        password: { label: "Password", type: "password" },
      },
      
      async authorize(credentials) { // call your external credentials validation service here/ hard coded for example.
        // console.log("credentials: ", credentials);
        
        const parsedCredentials = z 
          .object({
            email: z.string().email(),
            password: z.string().min(5),
          })
          .safeParse(credentials); // parse credentials as per defined schema

          // return user object if credentials are parsed without error to signin else return null to throw default error
        if (parsedCredentials.success) {
          const { email, password } = parsedCredentials.data;

          
          if(email=="admin@admin.com" && password=="admin"){
            const user = { email: email, name: "admin",image:"/default-avatar.jpg", role:'admin' };
            // console.log("user from func",user);
            
            return user;
            
          }else if (password == "123456") {
            const user = { email: email, name: "admin",image:"/default-avatar.jpg", role:'user' };
            return user;
          }
        }
        return null;
      },
    }),
  ],
});
