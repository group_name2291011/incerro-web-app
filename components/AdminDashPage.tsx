"use client";
import { useToast } from "@/context/ToastContext";
import { redirect } from "next/navigation";
import React, { useEffect } from "react";

const AdminDashPage = ({ role }: { role: "admin" | "user" }) => {
  const { showToast } = useToast();

  useEffect(() => {
    if (role !== "admin") {
      showToast("not authorized", "error");
      redirect("/");
    }
  }, []);

  if (role !== "admin") {
    return <div>Redirecting...</div>;
  }
  return <div>AdminDashPage</div>;
};

export default AdminDashPage;
