import { montserrat } from "@/utils/fonts";

const SectionHeading = () => {
  return (
    <h2
      className={`flex gap-2 w-full pt-16 px-4 md:pt-32 md:pl-12 lg:pl-24 ${montserrat.className} justify-center lg:justify-start `}
    >
      <span className="text-3xl md:text-5xl font-light capitalize text-gray text-center">
        specialized
      </span>
      <span className="text-3xl md:text-5xl text-white capitalize font-bold text-center">
        industries
      </span>
    </h2>
  );
};

export default SectionHeading;

("48  32");
