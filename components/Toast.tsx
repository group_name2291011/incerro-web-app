"use client";
import { useToast } from "@/context/ToastContext";


const ClientToast = ({ message }: { message: string }) => {
  const { visibility, msg, type } = useToast();

  return (
    visibility && (
      <div
        className={`fixed bottom-0 left-0 p-4 m-4 rounded-md bg-gray-800 text-white ${
          type == "error"
            ? "bg-red-600"
            : type == "success"
            ? "bg-lime-800"
            : "bg-fuchsia-400"
        }  transition-opacity duration-300`} 
      >
        {msg}
      </div>
    )
  );
};

export default ClientToast;
