import industriesData from "@/utils/data";
import IndustryCard from "./IndustryCard";


// this component maps over the array "industriesData" and render IndustryCard for each item
const IndustriesContainer = () => {
  return (
    <div className="grid grid-cols-1 items-start justify-center gap-6 md:gap-9 md:grid-cols-2 lg:grid-cols-3 px-4 md:px-12 lg:px-24">
      {industriesData.map((data) => (
        <IndustryCard key={data.title} data={data} />
      ))}
    </div>
  );
};

export default IndustriesContainer;
