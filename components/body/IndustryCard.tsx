"use client";
import React, { useEffect, useRef, useState } from "react";
import Image from "next/image";
import { openSans, poppins } from "@/utils/fonts";
import { IndustryCardPropType } from "@/utils/definitions";

const IndustryCard = ({ data }: { data: IndustryCardPropType }) => {
  const [visibility, setVisibility] = useState(false);
  const ref = useRef(null);

  // this useEffect is responsible for observing if the element is in viewport
  // set visibility to true if the element is in viewport to apply animation
  useEffect(() => {
    const scrollObserver = new IntersectionObserver(([el]) => {
      if (el.isIntersecting) {
        setVisibility(true);
        scrollObserver.unobserve(el.target);
      }
    });
    
    const targetElement = ref.current;
    if (targetElement) scrollObserver.observe(targetElement);

    return () => {
      if (targetElement) scrollObserver.unobserve(targetElement);
    };
  }, []);

  return (
    <div
      className={`
        ${data.id == 1 ? "md:col-span-2" : ""}
        ${visibility ? "opacity-100" : "opacity-0"}
        overflow-hidden flex flex-col gap-6  h-full
        transition-opacity duration-1000
        ` }
      ref={ref}
      
    >
      <div className="relative flex w-full justify-center overflow-hidden rounded-2xl aspect-video lg:aspect-auto lg:min-h-80 ">
        <Image
          src={data.image}
          alt={data.title}
          fill={true}
          placeholder="blur"
          sizes="(max-width: 768px) 100vw, (max-width: 1024px) 50vw, 33vw"
          className="w-full bg-cover bg-no-repeat object-cover h-auto"
        />
      </div>
      <div className="flex flex-col gap-2">
        <h4 className={`${poppins.className} text-white font-medium text-2xl`}>
          {data.title}
        </h4>
        <p className={`${openSans.className} text-gray font-normal text-base`}>
          {data.description}
        </p>
      </div>
    </div>
  );
};

export default IndustryCard;
