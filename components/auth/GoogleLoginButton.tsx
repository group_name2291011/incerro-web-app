import { googleAuthenticate } from '@/utils/authenticate';
import React from 'react'
import { useFormState } from 'react-dom';

const GoogleLoginButton = () => {
    const [errorMsgGoogle, dispatchGoogle] = useFormState(
        googleAuthenticate,
        undefined
      );
  return (
    <form className="flex flex-col" action={dispatchGoogle}>
        <button>Google Sign In</button>
        <p>{errorMsgGoogle}</p>
      </form>
  )
}

export default GoogleLoginButton