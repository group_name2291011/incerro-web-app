'use client'
import { useFormStatus } from 'react-dom';

const LoginButton = () => {
    const { pending } = useFormStatus();
    return (
      <button className="mt-4 w-full" aria-disabled={pending}>
        Log in
      </button>
    );
}

export default LoginButton


