"use client";

import { authenticate } from "@/utils/authenticate";
import { useFormState } from "react-dom";
import LoginButton from "./LoginButton";
import GoogleLoginButton from "./GoogleLoginButton";

const Login = () => {
    const [errorMessage, dispatch] = useFormState(authenticate, undefined);

    
  return (
    <>
      <form action={dispatch} className="space-y-3">
        <div>
          <h1 className={"mb-3 text-2xl"}>Please log in to continue.</h1>
          <div className="w-full">
            <label
              className="mb-3 mt-5 block text-xs font-medium text-gray-900"
              htmlFor="email"
            >
              Email
            </label>

            <input
              className="block w-full rounded-md border border-gray-200 text-sm outline-2 placeholder:text-gray-500"
              id="email"
              type="email"
              name="email"
              placeholder="Enter your email address"
              required
            />

            <label
              className="mb-3 mt-5 block text-xs font-medium text-gray-900"
              htmlFor="password"
            >
              Password
            </label>

            <input
              className="block w-full rounded-md border border-gray-200 text-sm outline-2 placeholder:text-gray-500"
              id="password"
              type="password"
              name="password"
              placeholder="Enter password"
              required
              minLength={6}
            />
          </div>
          <LoginButton />

          <div className="flex h-8 items-end space-x-1">
            {errorMessage && (
              <p className="text-sm text-red-500">{errorMessage}</p>
            )}
          </div>
        </div>
      </form>
      <GoogleLoginButton/>
      
    </>
  );
};

export default Login;
