import { auth } from "@/auth"
import Image from "next/image";


const UserDetails = async() => {

    const session = await auth()
    const {name, email, image} = session?.user!;
    
  return (
    <div className="flex flex-col justify-center gap-5 items-center border-4 p-8 rounded-md ">
        <div>
            <Image src={image!} width="100" height="100" alt={name!} priority />
        </div>
        <p className="font-bold text-xl"> {name}</p>
        <p>email: {email}</p>
        
    </div>
  )
}

export default UserDetails