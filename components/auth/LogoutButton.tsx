
import { signOut } from "@/auth";

// button to logout user
const LogoutButton =async () => {
    
  return (
    <form  action={async () => {
        'use server';
        await signOut();
      }}
    >
      <button className="w-full p-5 bg-blue-600 rounded-sm text-white font-bold" type="submit">Sign out</button>
    </form>
  )
}

export default LogoutButton