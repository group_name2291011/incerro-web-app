import ClientToast from "@/components/Toast";
import LogoutButton from "@/components/auth/LogoutButton";
import UserDetails from "@/components/auth/UserDetails";
import IndustriesContainer from "@/components/body/IndustriesContainer";
import SectionHeading from "@/components/heading/SectionHeading";
import Link from "next/link";



export default function Home() {
  return (
    <>
    <div className="min-h-screen flex flex-col justify-center items-center gap-9 ">
      <UserDetails/>
      <LogoutButton/>
      <Link href={'/admin'} className="border bg-slate-400 p-2">go to admin page</Link>
    </div>
    
    <main className="flex flex-col pb-32 gap-12 bg-black ">
      <SectionHeading/>
      <IndustriesContainer/>
    </main>
    <ClientToast message="you're not allowed here"/>
    </>
  );
}
