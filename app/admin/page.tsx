// "use client"
import { auth } from '@/auth';
import AdminDashPage from '@/components/AdminDashPage';
import { redirect } from 'next/navigation';
import React from 'react'

const AdminDashboard = async () => {
  const session = await auth()

  const {role} = session!;
  return (
    <AdminDashPage role={role}/>
  )
}

export default AdminDashboard;


/*
  toastContext > with Hook: toastType,Message, 

  toastComponent: useHook:{type, message, isVisible} > isVisible && <renderToast/>
*/